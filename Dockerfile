FROM node:16-alpine as build

WORKDIR /app

COPY package.json package-lock.json  ./

RUN npm ci --ignore-scripts

COPY . .

RUN npm run build

FROM node:16-alpine

ENV NODE_ENV production

WORKDIR /app


COPY --from=build /app/dist ./dist

#COPY --from=build /app/assets ./assets

COPY --from=build /app/package.json /app/package-lock.json /app/.env  ./

RUN npm ci --production --ignore-scripts

EXPOSE 3000

CMD ["npm", "run", "start:prod"]
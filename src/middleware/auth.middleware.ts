import * as admin from 'firebase-admin';

import { BadRequestException, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { NextFunction, Response } from 'express';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    public async use(req: any, _: Response, next: NextFunction): Promise<void> {
        const { authorization } = req.headers;
        if (!authorization) {
            throw new BadRequestException({ message: 'missing authorization header' });
        }
        const user = await this.authenticate(authorization);

        req.user = {
            email: user.email,
            type: user.type,
            uuid: user.user_id,
            displayName: user.displayName,
            email_verified: user.email_verified ?? false,
            phone_number: user.phone_number,
            customRoles: user.customRoles,
        };
        next();
    }

    private getToken(authToken: string): string {
        const match = authToken.match(/^Bearer (.*)$/);
        if (!match || match.length < 2) {
            throw new UnauthorizedException();
        }
        return match[1];
    }

    public async authenticate(authToken: string): Promise<any> {
        const tokenString = this.getToken(authToken);
        try {
            const decodedToken: admin.auth.DecodedIdToken = await admin.auth().verifyIdToken(tokenString);
            // eslint-disable-next-line @typescript-eslint/naming-convention
            const { email, user_id, name, email_verified, phone_number } = decodedToken;

            const { customClaims } = await admin.auth().getUser(user_id);

            return {
                email,
                user_id,
                customRoles: customClaims,
                displayName: name,
                email_verified,
                phone_number: phone_number.substring(1),
            };
        } catch (err) {
            throw new UnauthorizedException({
                message: err.message,
            });
        }
    }
}

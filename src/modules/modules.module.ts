import { AclModule } from './acl/acl.module';
import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';

@Module({
    imports: [AclModule, UserModule],
    providers: [],
    exports: [],
})
export class ModulesModule {}

import * as admin from 'firebase-admin';

import { Role } from '@common/enums/role.enum';
import { User } from '../entities/user.entity';

type ChangeRoleParams = {
    role: Role;
    storeId: string;
    user: admin.auth.UserRecord;
};

type GetAllUsersParams = {
    uid?: string;
    email: string;
    phoneNumber: string;
};
export class UserRepository {
    private fauth: admin.auth.Auth;

    constructor() {
        this.fauth = admin.auth();
    }

    public async getAllUsers(params: GetAllUsersParams[]) {
        const users = await this.fauth
            .getUsers(params)
            .then((value) => value)
            .catch((error) => error?.errorInfo);
        return users;
    }

    public async getUserByUid(uuid: string): Promise<admin.auth.UserRecord> {
        const user = await this.fauth
            .getUser(uuid)
            .then((value) => value)
            .catch((error) => error?.errorInfo);
        return user;
    }

    public async getUserByEmail(email: string) {
        const user = await this.fauth
            .getUserByEmail(email)
            .then((value) => value)
            .catch((error) => error?.errorInfo);
        return user;
    }

    public async createUser(userData: User) {
        const user = await this.fauth
            .createUser(userData)
            .then((value) => value)
            .catch((error) => error);
        return user;
    }

    public async updateUser(userId: string, userData: User) {
        const user = await this.fauth
            .updateUser(userId, userData)
            .then((value) => value)
            .catch((error) => error?.errorInfo);
        return user;
    }

    public async removeUser(userId: string) {
        return this.fauth
            .deleteUser(userId)
            .then((value) => {
                return { status: true };
            })
            .catch((error) => {
                return { status: false, error: error?.errorInfo };
            });
    }

    public async setRole({ role, storeId, user }: ChangeRoleParams) {
        const customClaims = user.customClaims ?? {};
        const customClaimsRole: string[] = [];

        if (role in customClaims) {
            customClaimsRole.push(...customClaims[role]);
        }

        if (!customClaimsRole.includes(storeId)) {
            customClaimsRole.push(storeId);
        }

        customClaims[role] = customClaimsRole;
        await this.fauth.setCustomUserClaims(user.uid, customClaims);
    }

    public async removeRole({ role, storeId, user }: ChangeRoleParams) {
        const customClaims: Record<string, string[]> = user.customClaims ?? {};
        if (role in customClaims) {
            const idx = customClaims[role].indexOf(storeId);
            customClaims[role].splice(idx, 1);
        }

        await this.fauth.setCustomUserClaims(user.uid, customClaims);
    }
}

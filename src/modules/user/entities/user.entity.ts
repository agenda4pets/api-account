import { Transform } from 'class-transformer';

export class User {
    email: string;

    password: string;

    emailVerified?: boolean = false;

    name: string;
    
    phoneNumber: string;

    displayName?: string;

    disabled?: boolean;

    photoURL?: string | null;
}

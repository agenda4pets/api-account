import { IsEnum, IsNotEmpty } from 'class-validator';

import { Role } from '@common/enums/role.enum';

export class RoleDto {
    @IsNotEmpty()
    @IsEnum(Role)
    role: Role;

    @IsNotEmpty()
    userId: string;

    @IsNotEmpty()
    storeId: string;
}

import { IsOptional } from 'class-validator';

export class GetAllUsersDto {
    @IsOptional()
    email: string;

    @IsOptional()
    name: string;

    @IsOptional()
    phoneNumber: string;
}

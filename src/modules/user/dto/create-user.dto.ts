import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

import { Role } from '@common/enums/role.enum';
import { StoreParamsDto } from '@common/dto/store-params.dto';
import { Transform } from 'class-transformer';

export class CreateUserDto extends StoreParamsDto {
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsOptional()
    password: string;

    @IsNotEmpty()
    name: string;

    @IsOptional()
    @Transform((value) => `+${value.value}`)
    phoneNumber: string;

    @IsOptional()
    roles: Role[];

    @IsOptional()
    displayName: string;

    @IsOptional()
    emailVerified?: boolean = false;
}

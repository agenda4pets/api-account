import { storeParamsInterceptor } from '@common/interceptors/store-params.interceptor';
import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseInterceptors } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { GetAllUsersDto } from './dto/get-all-users.dto';
import { RoleDto } from './dto/role.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserService } from './services/user.service';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get()
    public getAllUsers(@Query() getAllUsersDto: GetAllUsersDto) {
        return this.userService.getAllUsers(getAllUsersDto);
    }

    @Get(':userId')
    public getUserById(@Param('userId') userId: string) {
        return this.userService.getUserByUid(userId);
    }

    @Post()
    @UseInterceptors(storeParamsInterceptor())
    public createUser(@Body('data') createUserDto: CreateUserDto) {
        return this.userService.createUser(createUserDto);
    }

    @Put(':userId')
    @UseInterceptors(storeParamsInterceptor())
    public updateUser(@Param('userId') userId: string, @Body('data') updateUserDto: UpdateUserDto) {
        return this.userService.updateUser(userId, updateUserDto);
    }

    @Delete(':userId')
    public removerUser(@Param('userId') userId: string) {
        return this.userService.removeUser(userId);
    }

    @Post('roles')
    public async setRoleInUser(@Body('data') roleDto: RoleDto): Promise<any> {
        return this.userService.setRole(roleDto);
    }

    @Delete('roles')
    public async removeUserRole(@Body() roleDto: RoleDto): Promise<any> {
        return this.userService.removeUserRole(roleDto);
    }
}

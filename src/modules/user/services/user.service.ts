import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { GetAllUsersDto } from '../dto/get-all-users.dto';
import { RoleDto } from '../dto/role.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { UserRepository } from '../repositories/user.repository';

@Injectable()
export class UserService {
    constructor(
        private readonly userRepository: UserRepository
        
    ) {}

    async getAllUsers(getAllUserDto: GetAllUsersDto) {
        const users = await this.userRepository.getAllUsers([getAllUserDto]);
        if (users?.users.length < 1) {
            throw new HttpException(
                {
                    message: `Users was not found`,
                    status_code: 4004,
                    status: false,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return users;
    }

    async getUserByUid(uuid: string) {
        const user = await this.userRepository.getUserByUid(uuid);
        if (!user.uid) {
            throw new HttpException(
                {
                    message: `User was not found`,
                    status_code: 4004,
                    status: false,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return user;
    }

    async createUser({ storeId, ...createUserDto }: CreateUserDto) {
        let user = await this.userRepository.getUserByEmail(String(createUserDto.email));
        if (user?.uid) {
            return user;
        }

        user = await this.userRepository.createUser(createUserDto);

        if (createUserDto.roles?.length) {
            const setRolePromises = createUserDto.roles.map((role) =>
                this.setRole({
                    userId: user.uid,
                    storeId,
                    role,
                }),
            );
            await Promise.all(setRolePromises);
        }
        return user;
    }

    async updateUser(userId: string, { storeId, ...updateUserDto }: UpdateUserDto) {
        let user = await this.getUserByUid(userId);

        if (!user.uid) {
            throw new HttpException(
                {
                    message: 'Usuário não encontrado',
                    status_code: 4000,
                    status: false,
                },
                HttpStatus.BAD_REQUEST,
            );
        }

        user = await this.userRepository.updateUser(userId, updateUserDto);
        return user;
    }

    async removeUser(userId: string) {
        const user = await this.getUserByUid(userId);
        if (user.uid) {
            const remove = await this.userRepository.removeUser(user.uid);
            if (!remove.status) {
                throw new HttpException(
                    {
                        message: 'Erro ao cancelar Usuário',
                        status_code: 4204,
                        status: false,
                    },
                    HttpStatus.BAD_REQUEST,
                );
            }
        }

        throw new HttpException(null, HttpStatus.NO_CONTENT);
    }

    async setRole({ userId, storeId, role }: RoleDto): Promise<any> {
        const user = await this.getUserByUid(userId);

        this.userRepository.setRole({
            user,
            storeId,
            role,
        });
    }

    async removeUserRole({ userId, storeId, role }: RoleDto) {
        const user = await this.getUserByUid(userId);

        await this.userRepository.removeRole({
            user,
            storeId,
            role,
        });
    }
}

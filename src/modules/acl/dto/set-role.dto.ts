import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';

import { Role } from '@common/enums/role.enum';

export class SetRoleDto {
    @IsNotEmpty()
    @IsEnum(Role)
    role: Role;

    @IsEmail()
    user_email: string;
}

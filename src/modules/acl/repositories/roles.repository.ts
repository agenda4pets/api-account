import { BaseFirebaseRepository } from 'database/firebase/base-repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RolesRepository extends BaseFirebaseRepository {
    protected ref = '/roles';

    async getAllRoles(): Promise<any> {
        const roles = await this.getCollectionRef().get();
        return roles.docs.map((doc) => doc.data());
    }
}

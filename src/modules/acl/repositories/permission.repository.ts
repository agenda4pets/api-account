import { BaseFirebaseRepository } from 'database/firebase/base-repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PermissionRepository extends BaseFirebaseRepository {
    protected ref = '/permissions';

    async getAllPermission(): Promise<any> {
        const permissions = await this.getCollectionRef().get();
        return permissions.docs.map((doc) => doc.data());
    }
}

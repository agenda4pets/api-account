import { AclController } from './acl.controller';
import { AclService } from './services/acl.service';
import { Module } from '@nestjs/common';
import { PermissionRepository } from './repositories/permission.repository';
import { RolesRepository } from './repositories/roles.repository';

@Module({
    controllers: [AclController],
    providers: [AclService, PermissionRepository, RolesRepository],
})
export class AclModule {}

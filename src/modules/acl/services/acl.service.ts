import { Injectable } from '@nestjs/common';
import { PermissionRepository } from '../repositories/permission.repository';
import { RolesRepository } from '../repositories/roles.repository';

@Injectable()
export class AclService {
    constructor(private permissionRepository: PermissionRepository, private rolesRepository: RolesRepository) {}

    async findAllPermissionsAndRoles(): Promise<Record<'permissions' | 'roles', any[]>> {
        const [permissions, roles] = await Promise.all([
            this.permissionRepository.getAllPermission(),
            this.rolesRepository.getAllRoles(),
        ]);

        return { permissions, roles };
    }
}

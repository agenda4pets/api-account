import { Controller, Get, Post } from '@nestjs/common';

import { AclService } from './services/acl.service';

@Controller('acl')
export class AclController {
    constructor(private readonly aclService: AclService) {}

    @Get()
    findAllPermissionsAndRoles(): any {
        return this.aclService.findAllPermissionsAndRoles();
    }
}

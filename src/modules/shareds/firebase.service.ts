import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as admin from 'firebase-admin';
@Injectable()
export class FirebaseAuthService {
    private getToken(authToken: string): string {
        const match = authToken.match(/^Bearer (.*)$/);
        if (!match || match.length < 2) {
            throw new UnauthorizedException();
        }
        return match[1];
    }

    public async authenticate(authToken: string): Promise<any> {
        const tokenString = this.getToken(authToken);
        try {
            const decodedToken: admin.auth.DecodedIdToken = await admin.auth().verifyIdToken(tokenString);
            console.log(decodedToken);
            const { email, user_id, roles, name, email_verified, phone_number } = decodedToken;
            return {
                email,
                user_id,
                roles,
                displayName: name,
                email_verified,
                phone_number: phone_number.substring(1),
            };
        } catch (err) {
            console.log(`error while authenticate request ${err.message}`);
            throw new UnauthorizedException(err.message);
        }
    }
}
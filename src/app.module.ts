import { MiddlewareConsumer, Module, NestModule, OnApplicationBootstrap, RequestMethod } from '@nestjs/common';

import { APP_GUARD } from '@nestjs/core';
import { AclService } from '@account-acl/acl-service';
import { AppController } from './app.controller';
import { AuthMiddleware } from './middleware/auth.middleware';
import { ConfigModule } from '@nestjs/config';

import { FirebaseModule } from './database/firebase/firebase.module';
import { ModulesModule } from '@modules/modules.module';

import { PermissionGuard } from '@account-acl/common/guards';
import { RouteInfo } from '@nestjs/common/interfaces';
import { aclProvider } from '@account-acl/common/adapters/nestjs.provider';
import { FirebaseAuthService } from '@modules/shareds/firebase.service';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        FirebaseModule,
        ModulesModule,
    ],
    controllers: [AppController],
    exports: [FirebaseAuthService],
    providers: [
        FirebaseAuthService,
        {
            provide: APP_GUARD,
            useClass: PermissionGuard,
        },
        aclProvider(),
    ],
})
export class AppModule implements NestModule, OnApplicationBootstrap {
    constructor(private aclService: AclService) {}

    async onApplicationBootstrap(): Promise<void> {
        this.aclService.init({
            endpoint: {
                url: `${process.env.ACL_URL}/acl`,
                headers: {},
            },
        });
    }

    public configure(consumer: MiddlewareConsumer): void {
        const routesToExclude: RouteInfo[] = [
            {
                path: 'acl',
                method: RequestMethod.GET,
            },
            {
                path: 'user/role',
                method: RequestMethod.POST,
            },
        ];
        consumer
            .apply(AuthMiddleware)
            .exclude(...routesToExclude)
            .forRoutes({ path: '*', method: RequestMethod.ALL });
    }
}

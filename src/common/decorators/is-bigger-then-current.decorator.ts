import { registerDecorator, ValidationArguments } from 'class-validator';
import { DateUtil } from '@common/utils/date.util';

export function IsBiggerThanCurrentDt() {
    return (object: any, propertyName: any): any => {
        registerDecorator({
            name: 'IsBiggerThanCurrentDt',
            target: object.constructor,
            propertyName,
            constraints: [null],
            options: null,
            validator: {
                validate(value: any) {
                    const dtValue = DateUtil.createDateAsUTC(new Date(Date.parse(value)));
                    const dtNow = DateUtil.createDateAsUTC();
                    if (dtNow.getTime() >= dtValue.getTime()) {
                        return false;
                    }

                    const diffHours = ~~((dtNow.getTime() - dtValue.getTime()) / 1000 / 60 / 60);

                    dtValue.setHours(dtValue.getHours() + 1);
                    if (dtNow.getTime() < dtValue.getTime() && diffHours === 0) {
                        return false;
                    }

                    return true;
                },

                defaultMessage(args: ValidationArguments) {
                    return `The ${args.property} must be greater than the current date`;
                },
            },
        });
    };
}

import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsPermitIfNotInformed(property: string, validationOptions?: ValidationOptions) {
    return (object: any, propertyName: any): any => {
        registerDecorator({
            name: 'IsPermitIfNotInformed',
            target: object.constructor,
            propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [releatedAttribute] = args.constraints;
                    if (value !== undefined && args.object[releatedAttribute] !== undefined) {
                        return false;
                    }
                    return true;
                },

                defaultMessage(args: ValidationArguments) {
                    return `${args.property} is informed, you can not inform ${args.constraints[0]}`;
                },
            },
        });
    };
}

import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function isDateBeforeThan(property: string, validationOptions?: ValidationOptions) {
    return (object: any, propertyName: any): any => {
        registerDecorator({
            name: 'isDateBeforeThan',
            target: object.constructor,
            propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const dtStart = new Date(Date.parse(value));
                    const dtEnd = new Date(Date.parse(args.object[args.constraints[0]]));

                    if (dtStart.getTime() >= dtEnd.getTime()) {
                        return false;
                    }
                    return true;
                },

                defaultMessage(args: ValidationArguments) {
                    return `The ${args.property} must be less than the ${args.constraints[0]}`;
                },
            },
        });
    };
}

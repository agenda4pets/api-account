import { DateUtil } from '@common/utils/date.util';
import { registerDecorator, ValidationArguments } from 'class-validator';

export function isIsoDate() {
    return (object: any, propertyName: any): any => {
        registerDecorator({
            name: 'isIsoDate',
            target: object.constructor,
            propertyName,
            constraints: [null],
            options: null,
            validator: {
                validate(value: any) {
                    return DateUtil.isIsoDate(value);
                },

                defaultMessage(args: ValidationArguments) {
                    return `The ${args.property} must be date ISO8601`;
                },
            },
        });
    };
}

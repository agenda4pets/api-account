import { AclPermission } from '@account-acl/common/decorators/permission.decorator';
import { Permissions } from '@common/enums/permissions.enum';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function AppPermissions(permission: Permissions) {
    return AclPermission(permission);
}

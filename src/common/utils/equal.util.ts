export class EqualUtils {
    static isDeepEqual(object1: any, object2: any): boolean {
        const keys1 = Object.keys(object1);
        const keys2 = Object.keys(object2);

        if (keys1.length != keys2.length) return false;

        for (const key of keys1) {
            const value1 = object1[key];
            const value2 = object2[key];
            if (object1[key] !== object2[key] || value1 !== value2) {
                return false;
            }
        }

        return true;
    }
}

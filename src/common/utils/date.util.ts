export class DateUtil {
    static isDate(date: string): boolean {
        if (!date.match(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)) {
            return false;
        }
        const dateValidation = Date.parse(date);
        if (Number.isNaN(dateValidation)) {
            return false;
        }
        return true;
    }

    static createDateAsUTC(date?: Date): Date {
        let dateValue = date;
        if (dateValue === undefined) {
            dateValue = new Date();
        }
        return new Date(
            Date.UTC(
                dateValue.getFullYear(),
                dateValue.getMonth(),
                dateValue.getDate(),
                dateValue.getHours(),
                dateValue.getMinutes(),
                dateValue.getSeconds(),
            ),
        );
    }

    static isIsoDate(date?: string): boolean {
        if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(date)) return false;
        try {
            const d = new Date(date);
            return d.toISOString() === date;
        } catch (error) {
            return false;
        }
    }
}

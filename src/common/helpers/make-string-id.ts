export default function makeStringId(length: number): string {
    let res = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        res += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return res;
}

export function makeObjectId(): string {
    const timestamp = ((new Date().getTime() / 1000) | 0).toString(16);
    return `${timestamp}xxxxxxxxxxxxxxxx`.replace(/[x]/g, () => ((Math.random() * 16) | 0).toString(16));
}

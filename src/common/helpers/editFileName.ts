import { extname } from 'path';
import * as fs from 'fs';
import { HttpException, HttpStatus } from '@nestjs/common';
import typeImports from './imports';

export default function (req: any, file: any, callback: any): any {
    const fullPath = `${req.headers['x-tenant-id']}/${req.headers['x-channel-id']}/${req.body.sellerId}`;
    if (!typeImports.includes(req.params.typeImport)) {
        callback(
            new HttpException(
                `The URI does not have a path valid for uploads/${req.body.sellerId}/${req.params.typeImport}`,
                HttpStatus.BAD_REQUEST,
            ),
            false,
        );
        return;
    }
    if (!fs.existsSync(process.env.STORAGE + fullPath)) {
        fs.mkdirSync(process.env.STORAGE + fullPath, { recursive: true });
    }

    const fileExtName = extname(file.originalname);
    callback(null, `${fullPath}/${req.params.typeImport}-${new Date().getTime()}${fileExtName}`);
}

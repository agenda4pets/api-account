export default (headers: Record<string, any>): { tenantId: string; channelId: string; correlationId: string } => {
    const tenantId = headers['x-tenant-id'];
    const channelId = headers['x-channel-id'];
    const correlationId = headers['x-correlation-id'];

    return { tenantId, channelId, correlationId };
};

import * as fs from 'fs';
export default (path: string): any => {
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
    }
};

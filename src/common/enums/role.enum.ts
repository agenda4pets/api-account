export enum Role {
    CLIENT = 'client',
    STORE_OWNER = 'store-owner',
    STORE_EMPLOYEE = 'store-employee',
    STORE_EMPLOYEE_DOCTOR = 'store-employee-doctor',
    ADMIN = 'admin',
}

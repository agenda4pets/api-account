export enum ResultEnum {
    created = 'created',
    processed = 'processed',
    updated = 'updated',
    deleted = 'deleted',
    failed = 'failed',
}

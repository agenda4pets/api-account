import { IsNotEmpty } from 'class-validator';

export class StoreParamsDto {
    @IsNotEmpty()
    storeId: string;
}

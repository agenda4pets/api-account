import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';

import { Observable } from 'rxjs';
import { Request } from 'express';

type QueryOrBody = 'body' | 'query';
@Injectable()
class StoreParamsInterceptor implements NestInterceptor {
    constructor(private appendTo: QueryOrBody) {}

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const request = context.switchToHttp().getRequest<Request>();
        request[this.appendTo] = {
            ...request[this.appendTo],
            storeId: request.headers['x-store-id'],
        };
        return next.handle();
    }
}

export function storeParamsInterceptor(appendTo: QueryOrBody = 'body') {
    return new StoreParamsInterceptor(appendTo);
}

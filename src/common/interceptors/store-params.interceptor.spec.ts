import { storeParamsInterceptor } from './store-params.interceptor';

describe('StoreParamsInterceptor', () => {
    it('should be defined', () => {
        expect(storeParamsInterceptor()).toBeDefined();
    });
});

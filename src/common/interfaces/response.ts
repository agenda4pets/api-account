import { ResultEnum } from '../enums/result.enum';

export type ResponseCategory = { categoryId: number; result: string; error?: string | any[] };
export type ResponseSku = { skuId: string; result: string; error?: string | any[] };

export interface ResponseType {
    id?: number | string;
    skuId?: string;
    affectedSkus?: number;
    skus?: ResponseSku[];
    categories?: ResponseCategory[];
    sellerId: string;
    result: ResultEnum;
    error?: string | any[];
}
export interface Paginate {
    recordsReturned: number;
    totalPages: number;
    totalRecords: number;
    currentPage: number;
    lastPage?: number;
    nextPage?: number;
    prevPage?: number;
}
export interface DataLiteral<T> {
    [x: string]: T[];
}

type DataType<T> = DataLiteral<T>;
type DateTypePaginate<T> = DataType<T> & { paging: Paginate };

export interface ResultAction<T> {
    data: DataType<T>;
}
export interface ResultActionPaging<T> {
    data: DateTypePaginate<T>;
}

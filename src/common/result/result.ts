export class Result {
    constructor(
        public data?: any,
        public paging?: any,
        public result?: string,
        public error?: any,
        public status_code?: any,
    ) {}
}

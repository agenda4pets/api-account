import * as dotenv from 'dotenv';
import { AppModule } from './app.module';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

dotenv.config();

async function bootstrap() {
    //const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
    const app = await NestFactory.create(AppModule, {
        bodyParser: true,
    });

    // await swaggerConfig(app, []);

    app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

    const port = process.env.PORT || 3000;
    await app.listen(port).then(() => console.log(`Application listening on port ${port}`));
}

bootstrap();

import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger';

const APP_NAME = process.env.npm_package_name;
const APP_VERSION = process.env.npm_package_version;
const PORT_API = process.env.PORT;

export const swaggerConfig = async function conf(app: INestApplication, modules: any[]): Promise<void> {
    const config = new DocumentBuilder()
        .setTitle(APP_NAME)
        .setDescription(`The ${APP_NAME} API description`)
        .setVersion(APP_VERSION)
        .setContact('Arquitetura de Solução', 'http://www.agenda4pets.com.br', 'contato@agenda4pets.com.br')
        .addServer('http://localhost:' + PORT_API)
        .addBearerAuth()
        .build();

    const options: SwaggerDocumentOptions = {
        operationIdFactory: (controllerKey: string, methodKey: string) => methodKey,
        include: modules,
    };
    const document = SwaggerModule.createDocument(app, config, options);
    SwaggerModule.setup('swagger', app, document);
};

import * as admin from 'firebase-admin';

export class BaseFirebaseRepository {
    protected ref: string;

    protected getCollectionRef(): admin.firestore.CollectionReference {
        return admin.firestore().collection(this.ref);
    }
}
